# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

## Summary from session 16

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Databas migration

It's best practice to not modify database with psql instantly, use migration instead to track what has changed and easily revert when fail happened.

Migration keep database from multi environment to be synchronized. From development into production.

### Bcrypt

When we storing user data, **Never keep raw string value of password into database**.

We use bcrypt to hash password, and then insert that hashed password safely.

Use `compare()` from bcrypt methods to check does the credentials is correct or not.

## Task

Completed a validation from User model. Added a `beforeCreate()` hooks so the hash happened in model, not in the controllers.

:file_folder: Model file [user.js](../Apps/models/user.js)

I also made a migration to change `email` and `encrypted_password` columnt constraint to be not allowed null values in databases.

Migrations file :

:file_folder: [1st migration](../Apps/migrations/20200513154241-migration-skeleton.js)

and the other with `transactions()` methods

:file_folder: [2nd migration](../Apps/migrations/20200513160719-transaction-test.js)
