"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn("users", "encrypted_password", {
          type: Sequelize.STRING,
          allowNull: false,
        }),
        queryInterface.changeColumn("products", "name", {
          type: Sequelize.STRING,
          allowNull: false,
        }),
        queryInterface.changeColumn("products", "price", {
          type: Sequelize.BIGINT,
          allowNull: false,
        }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn("users", "encrypted_password", {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.changeColumn("products", "name", {
        type: Sequelize.STRING,
        allowNull: true,
      }),
    ]);

    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
};
