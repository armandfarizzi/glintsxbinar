"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    // return queryInterface.addConstraint('users', {

    // })
    return queryInterface.changeColumn("users", "email", {
      type: Sequelize.STRING,
      allowNull: false, // note this
    });
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */

    return queryInterface.changeColumn("users", "email", {
      type: Sequelize.STRING,
      allowNull: true,
    });
  },
};
