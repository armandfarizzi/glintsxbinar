"use strict";
const { hashSync, genSaltSync, compareSync } = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        /*
         *  Validate -> validate the type met condition before create, save, & update
         *  isEmail -> has email format
         *
         * */
        validate: {
          isEmail: true,
          notNull: true,
        },
      },
      encrypted_password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: true,
          len: [8, 20],
        },
      },
      verified: {
        type: DataTypes.BOOLEAN,
      },
    },
    {
      underscored: true,
    }
  );
  User.associate = function (models) {
    // associations can be defined here
  };

  User.beforeCreate((user, options) => {
    const salt = genSaltSync(10);
    const hashedPassword = hashSync(user.encrypted_password, salt);
    user.encrypted_password = hashedPassword;
  });

  User.checkCredentials = (password, user) => {
    return compareSync(password, user.encrypted_password);
  };
  return User;
};
