const express = require("express");
const router = express.Router();
const product = require("./controllers/productController");
const user = require("./controllers/userController");

/* Product API Collection */
router.get("/products", product.all);
router.get("/products/available", product.available);
router.get("/products/:name", product.find);
router.post("/products", product.create);
router.patch("/products/:id", product.update);
router.delete("/products", product.delete);

/* User API Collection */
router.get("/users", user.all);
router.get("/users/:id", user.find);
router.post("/users", user.create);
router.patch("/users/:id", user.update);
router.delete("/users/:id", user.delete);
router.post("/users/login", user.login);
module.exports = router;
