const { Product } = require("../models");

module.exports = {
  all(req, res) {
    Product.findAll()
      .then((data) => {
        res.status(200).json({
          status: "success",
          data,
        });
      })
      .catch((error) => {
        res.status(400).json({
          status: "fail",
          data: error.message,
        });
      });
  },
  async find(req, res) {
    let founditem = await Product.findOne({ where: { name: req.params.name } });
    if (founditem) {
      res.status(200).json({
        status: "success",
        data: founditem,
      });
    } else {
      res.status(404).json({
        status: "fail",
        message: "Item not found.",
      });
    }
  },
  update(req, res) {
    Product.update(req.body, { where: { id: req.params.id } })
      .then(async (item) => {
        res.json({ status: "true", data: item });
      })
      .catch(() => {
        res.status(404).json({
          status: "fail",
          message: "Item not found.",
        });
      });
  },
  create(req, res) {
    Product.findOrCreate({
      where: { name: req.body.name },
      defaults: { price: req.body.price, stock: req.body.stock },
    })
      .then(([product, created]) => {
        if (created) {
          res.status(200).json({
            status: "success",
            data: product,
          });
        } else {
          res.status(400).json({
            status: "fail",
            message: "Item already exist.",
          });
        }
      })
      .catch((error) => {
        res.status(400).json({ status: "failed", message: error.message });
      });
  },
  delete(req, res) {
    Product.findOne({ where: { name: req.body.name } })
      .then((item) => {
        item.destroy().then((a) => {
          res.json({ status: "success", message: "deleted" });
        });
      })
      .catch(() => {
        res.json({ status: "fail", message: "no item" });
      });
  },
  async available(req, res) {
    let allitems = await Product.findAll();
    let availableItems = allitems.filter((i) => i.stock != 0);
    res.json({ status: true, availableItems });
  },
};
