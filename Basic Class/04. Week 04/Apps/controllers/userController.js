const { User } = require("../models");
const bcrypt = require("bcryptjs");

module.exports = {
  all(req, res) {
    User.findAll()
      .then((data) => {
        res.status(200).json({
          status: "success",
          data,
        });
      })
      .catch((error) => {
        res.status(400).json({
          status: "fail",
          data: error.message,
        });
      });
  },
  async find(req, res) {
    let foundUser = await User.findOne({ where: { id: req.params.id } });
    User.checkCredentials("TEST", foundUser);
    if (foundUser) {
      res.status(200).json({
        status: "success",
        data: foundUser,
      });
    } else {
      res.status(404).json({
        status: "fail",
        message: "Item not found.",
      });
    }
  },
  update(req, res) {
    User.findOne({ where: { id: req.params.id } })
      .then(async (item) => {
        item.email = req.body.email || item.email;
        item.encrypted_password = req.body.password
          ? "encrypt-this-" + req.body.password
          : item.encrypted_password;
        await item.save();
        res.json({ status: "true", data: item });
      })
      .catch(() => {
        res.status(404).json({
          status: "fail",
          message: "Item not found.",
        });
      });
  },

  login(req, res) {
    User.findOne({ where: { email: req.body.email } })
      .then((foundUser) => {
        let result = User.checkCredentials(req.body.password, foundUser);
        res
          .status(result ? 200 : 404)
          .json(
            result
              ? { status: "success", message: "Password is true" }
              : { status: "fail", message: "password wrong" }
          );
      })
      .catch((e) => {
        res.status(404).json({ status: "fail", message: e.message });
      });
  },
  create(req, res) {
    User.findOrCreate({
      where: { email: req.body.email },
      defaults: {
        encrypted_password: req.body.password,
        verified: false,
      },
    })
      .then(([user, created]) => {
        if (created) {
          res.status(200).json({
            status: "success",
            data: user,
          });
        } else {
          res.status(400).json({
            status: "fail",
            message: "Item already exist.",
          });
        }
      })
      .catch((error) => {
        res.status(400).json({ status: "failed", message: error.message });
      });
  },
  delete(req, res) {
    User.findOne({ where: { id: req.params.id } })
      .then((item) => {
        item.destroy().then((a) => {
          res.json({ status: "success", message: "deleted" });
        });
      })
      .catch(() => {
        res.json({ status: "fail", message: "no item" });
      });
  },
};
