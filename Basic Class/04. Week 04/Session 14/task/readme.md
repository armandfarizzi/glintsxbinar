### Explanation - Daily Task 14

## Using express to serve an endpoints for certain geometry calculation   

At this task, i use router-level middleware from express that allowing the server to divide each request with a controller

Basically, to receive a valid request to calculation controller, request body must be contained a matching keys. the keys is listed like below

| No  | Name         | Keys                | Body example                        |
| --- | ------------ | ------------------- | ----------------------------------- |
| 1   | circleArea   | "radius"            | ```{ "radius" : 5 }   ```           |
| 2   | squareArea   | "side"              | ```{ "side" : 5 }```                |
| 3   | cubeVolume   | "side"              | ```{ "side" : 7 }```                |
| 4   | tubeVolume   | "radius" , "height" | ```{ "radius" : 5, "height" : 3}``` |
| 5   | triangleArea | "length", "height"  | ```{ "length" : 8, "height" : 3}``` |

If a request doesn't complete the keys needed as the list above, server will return a 400 bad request status code with error message like this :
 ```
 { "message" : "argument missing!"}
 ```

Express - App in Action :

triangleArea :

![triangleArea](../../../../.assets/task-14-1.png)

tubeVolume :

![tubeVolume](../../../../.assets/task-14-2.png)

