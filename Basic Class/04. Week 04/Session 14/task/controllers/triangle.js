module.exports = (req, res) => {
  if(typeof req.body.height == "number" && typeof req.body.length == "number"){
    let triangleArea = req.body.height * req.body.length / 2
    res.status(201).json({triangleArea: triangleArea})
  } else {
    res.status(400).json({message: "argument missing!"})
  }
}