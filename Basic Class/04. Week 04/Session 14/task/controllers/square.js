module.exports = (req, res) => {
  if(typeof req.body.side == "number"){
    let squareArea = (req.body.side ** 2)
    res.status(201).json({squareArea: squareArea})
  } else {
    res.status(400).json({message: "argument missing!"})
  }
}
