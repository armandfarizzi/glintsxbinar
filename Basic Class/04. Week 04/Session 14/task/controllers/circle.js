module.exports = (req, res) => {
  if(typeof req.body.radius == "number"){
    let circleArea = (req.body.radius ** 2) * 3.14159
    res.status(201).json({circleArea: circleArea})
  } else {
    res.status(400).json({message: "argument missing!"})
  }
}