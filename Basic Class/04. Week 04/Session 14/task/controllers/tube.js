module.exports = (req, res) => {
    if(typeof req.body.height == "number" && typeof req.body.radius == "number"){
    let tubeVolume = req.body.height * (req.body.radius ** 2 * 3.14159);
    res.status(201).json({tubeVolume: tubeVolume});
  } else {
    res.status(400).json({message: "argument missing!"});
  }
}
