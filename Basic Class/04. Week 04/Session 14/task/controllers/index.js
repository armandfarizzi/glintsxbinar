module.exports = {
  circle: require("./circle.js"),
  cube: require("./cube.js"),
  square: require("./square.js"),
  triangle: require("./triangle.js"),
  tube: require("./tube.js"),
}