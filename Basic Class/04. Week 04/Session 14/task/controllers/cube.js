module.exports = (req, res) => {
  if(typeof req.body.side == "number"){
    let cubeVolume = (req.body.side ** 3)
    res.status(201).json({cubeVolume: cubeVolume})
  } else {
    res.status(400).json({message: "argument missing!"})
  }
}