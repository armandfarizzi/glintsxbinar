// Route Level Middleware
const router = require('express').Router();

// Import Controller
const { circle, tube, cube, square, triangle, info  } = require('./controllers');

router.post('/circleArea', circle);
router.post('/tubeVolume', tube);
router.post('/cubeVolume', cube);
router.post('/squareArea', square);
router.post('/triangleArea', triangle);

// router.get('/users', getUser);

module.exports = router;
