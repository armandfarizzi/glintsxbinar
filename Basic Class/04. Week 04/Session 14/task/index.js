const express = require('express');
const cors = require('cors');

const app = express();
const port = 3000;

const router = require('./router');

// Data
// const users = require('./db/users.json');
// const products = require('./db/products.json');

// Middleware
app.use(cors()); // CORS to allow front-end use our server
app.use(express.json ()); // JSON parser

// GET /
app.get('/', (req, res) =>
  res.json({
    status: true,
    message: 'Hello World!',
  })
);
// GET /info
app.get('/info', (req,res) => {
  res.status(200).json({
    "status": true,
    "data": [
      "POST /calculate/circleArea",
      "POST /calculate/squareArea",
      "POST /calculate/triangleArea",
      "POST /calculate/cubeVolume",
      "POST /calculate/tubeVolume",
    ]
  })
})


app.use('/calculate', router);

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
