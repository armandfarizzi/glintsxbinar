# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png 'Glints Academy')

## Summary from session 14

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### HTTP Server:

HTTP server is meant to serve data (could be XML format for SOAP Architecture or JSON format for REST Architecture) to the client. Clients is basically any apps that consume API to working with. Every connection between client-server must be containing a request and response in order to exchange data. In reality we can see a URL as a request and a HTML documents given later as a respond from the HTTP server.

### HTTP Status Code:

Status code is a code that contain three digits embedded at response from the server. Status code give an information to the client about what happened to the request inside the server. Server could accept, forbid, redirect and every action is listed as below :

| No  | Response               | Status (Begin with) | Body example                                               |
| --- | ---------------------- | ------------------- | ---------------------------------------------------------- |
| 1   | Information responses  | 1xx                 | 100 : Continue                                             |
| 2   | Successful responses   | 2xx                 | 200 : OK \| 201 : Created                                  |
| 3   | Redirection messages   | 3xx                 | 300: Multiple choice \| 302 : Found                        |
| 4   | Client error responses | 4xx                 | 400 : Bad request \| 401 : Unauthorized \| 403 : Forbidden |
| 5   | Server error responses | 5xx                 | 500 : Internal server error                                |

### Modular System

In node.js most of the time we working with external module that acquired from package manager. To start using external module we begin calling **npm init** in a terminal. And then an interactive command will begin to form a package.json file which contain a basic information about application that we going to build there

```
$ npm init
```

To install a package we type **npm install \<pkgname>** in a terminal. Wait npm to fetch the module, then check the **node_modules** folder to ensure the package is exist and ready to be used afterwards.

```
$ npm install <pkgname>
```

## Task

Completed a Express aplication having an endpoint to serve a geometry volume and area calculation [See more..](./task/readme.md)
