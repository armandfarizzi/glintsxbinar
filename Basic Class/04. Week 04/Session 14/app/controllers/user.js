const users = require('../db/users.json');

module.exports = {
  getUser: (req, res) => {
    const user = { ...users };
    delete user.password;

    res.status(200).json(user);
  },

  login: (req, res) => {
    if (
      users.email === req.body.email &&
      users.password === req.body.password
    ) {
      res.status(201).json({
        status: true,
        message: 'Succesfully logged in!',
      });
    } else {
      res.status(401).json({
        status: false,
        errors: 'Invalid credentials',
      });
    }
  },
};
