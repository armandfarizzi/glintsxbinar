const products = require('../db/products.json');

module.exports = {
  getProducts: (req, res) => {
    res.status(201).json(products);
  },

  postProducts: (req, res) => {
    res.status(201).json({
      name: req.body.name,
      message: `${req.body.name} are ${
        req.body.price > 4000 ? 'Expensive' : 'Cheap'
      }`,
    });
  },
};
