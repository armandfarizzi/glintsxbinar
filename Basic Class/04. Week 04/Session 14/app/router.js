// Route Level Middleware
const router = require('express').Router();

// Import Controller
const { getProducts, postProducts } = require('./controllers/product');
const { getUser, login } = require('./controllers/user');

/* Product API Collection */
router.get('/products', getProducts);
router.post('/products', postProducts);

/* User API Collection */
router.get('/users', getUser);
router.post('/login', login);

module.exports = router;
