const products = require('./products.json');
const users = require('./users.json');

/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */

const http = require('http');

const handler = (req, res) => {
  switch (req.url) {
    case '/':
      res.writeHead(200);
      res.write(
        JSON.stringify({
          status: true,
          message: 'Hello World!',
        })
      );

    case '/products':
      res.writeHead(200);
      res.write(JSON.stringify(products, null, 2));

    case '/products/available':
      res.writeHead(200);
      res.write(
        JSON.stringify(
          products.filter((i) => i.stock > 0),
          null,
          2
        )
      );

    case '/users':
      let user = { ...users };
      delete user.password;
      res.writeHead(200);
      res.write(JSON.stringify(user));

    default:
      res.writeHead(404);
      res.write(
        JSON.stringify({
          status: false,
          errors: 'Not Found!',
        })
      );
  }

  res.end();
};

const app = http.createServer(handler);
app.listen(3000, () => console.log(`Server is running on port ${3000}`));
