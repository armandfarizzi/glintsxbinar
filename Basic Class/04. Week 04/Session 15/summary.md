# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

## Summary from session 15

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### SQL - DBMS - Sequelize:

SQL is a query language

DBMS is a software that used to managing database

Sequelize is a node.js driver that allowing us to build, manage, edit, and all operation relating to sql database using exported model.

### Installing and connecting postgres:

Login as postgres -> create a user with corresponding pc name

Exit postgres, and then we can use psql cmd as pc name given before

## Task

Completed a CRUD Express with Sequelize application. The application have 2 model which is User & Product

:file_folder: [Application](../Apps/) in action :

Get all product

![getallproduct](../../../.assets/task-15-1.png)

Get available product

![getavaproduct](../../../.assets/task-15-2.png)

Create user

![createuser](../../../.assets/task-15-3.png)

Delete user

![deleteuser](../../../.assets/task-15-4.png)
