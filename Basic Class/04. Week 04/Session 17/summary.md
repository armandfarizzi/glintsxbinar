# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

## Summary from session 17

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Json Web Token

[JSON Web Token](https://tools.ietf.org/html/rfc7519) (JWT) is a compact, URL-safe means of representing
claims to be transferred between two parties. The claims in a JWT
are encoded as a JSON object that is used as the payload of a JSON
Web Signature (JWS) structure or as the plaintext of a JSON Web
Encryption (JWE) structure, enabling the claims to be digitally
signed or integrity protected with a Message Authentication Code
(MAC) and/or encrypted.

Before using JWT we need to install the package to our working repository by running ```$ npm install jsonwebtoken``` and require the package at the file we needed. JWT need a secretOrPublicKey that supplied whenever sign or verify is executed which usually stored in an .env file

### Sign, Verify

```sign()``` is a jsonwebtoken method that used to create a new token. Meanwhile ```verify()``` is a method that used to extract the payload from the token given. By the name itself ```sign()``` is purposed when the user logged in, and ```verify()``` is used to check the request credentials when accessing a protected routes.

## Task

Check [apps-session17](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session17) branch
