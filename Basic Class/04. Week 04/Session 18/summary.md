# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

## Summary from session 18

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Rules of writing a clean code

# [Don't Repeat Yourself](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)

If there is a code line that repeated and used in many elements of your apps, we are suggested to refactor it for increasing accessibility and mantainability of the code. In our express app, the authentication approach is to define the function in every action controller, and thus made the code repeated in every action. The solution is we write in one middleware called `auth` and require it to the top of the code, and call it everywhere we want.

### Error Handler

Processing an error is required to build an informative applications. Sometimes an unhandled error may cause an uncomfortable user experience, isn't it?

An error that could happen in the applications is derived from client error which having 4xx status code and server error which having 5xx status code. To handle an error, we can apply an extra [middleware](https://gitlab.com/armandfarizzi/glintsxbinar/-/blob/apps-session18/middlewares/errorHandler.js) which positioned after all the routes registered in our apps. And put `next(err)` inside a `catch..` blocks. It is applied in apps-session18 branch inside [productController.js](https://gitlab.com/armandfarizzi/glintsxbinar/-/blob/apps-session18/controllers/productController.js)

## Task

Check [apps-session18](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session18) branch
