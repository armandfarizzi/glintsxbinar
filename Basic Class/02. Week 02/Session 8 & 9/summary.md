# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 8 & 9
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Loop :
- For loop
- while loop
- do while loop

### Array : 
- looping through array with for loop vs forEach method
- array mutation
- array built-in method :
    - ```arrayName.map()```
    use case : modifying each element
    - ```arrayName.forEach()```
    use case : doing something through each element
    - ```arrayName.filter()```
    use case : filtering array with condition given
    - ```arrayName.sort()```
    use case : sorting an array to Ascending or Descending based argument given 

### Algorithm :
- filtering value
- exchanging value

### Node JS
- Import ```require('./pathofexported.js')```
- Export ```module.exports = exportSomething```
- Object data type :
    - clone object using spread operator ```{ ...objname }```
    - autoname

## Task
Filter an array of object that contain ```null``` value

```
function clean(data) {
    let result = [];
    data.forEach(a => {
        if(a.name !=null && a.luckyNumber!=null){
            result.push(a)
        }
    });
    return result;
}
```
alternatives
```
function clean(data) {
    return data.filter(a => (a.name != null) && (a.luckyNumber != null)); // one line answer with array built-in method  
}
```
### Clean data in action
![clean data](../../../.assets/task_5_preview.png)

