
const data = [];
const randomNumber = Math.floor(Math.random() * 100);

function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement())
  }

  // Recursive
  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);

  return [null, random, random][Math.floor(Math.random() * 3)]
}

createArray()

/*
 * Code Here!
 * */
console.log(data);

function clean(data) {
  // Code here

  /*
   *  Cleaning Array :
   *  1. Loop Through Array
   *  2. Compare Array element to null
   *  3. Push to the new Array if element is not a null
   *  4. return the newly array of not null
   * */
  let ans = []
  for(let i=0; i<data.length; i++){
    if(data[i] !== null){
      ans.push(data[i])
    }
  }
	return ans
}

/*
 * DON'T CHANGE
 * */

if (process.argv.slice(2)[0] == "test") {
  try {
    clean(data).forEach(i => {
      if (i == null) {
        throw new Error("Array still contains null")
      }
    })
  }

  catch(err) {
    console.error(err.message);
  }
}

