# Code Challenge 03

### Web Api With Node.js HTTP Module

HTTP Module is a built-in node.js package that allowing us to handle every http action such as requesting to a url, creating a server and many more.

At this challenge I created a server with http module to serve a data with corresponding endpoints. 

With ```switch()``` statement i map every requested url with a method ```writeResponse()``` which accept an objects as an arguments resulting in json will be delivered to the users.

I use ```string.toLowerCase()``` method to match case-insensitive requested url and also it doesn't matter if the url ends with ```/``` (backslash) or not since i bind them into the same actions. 

[Source Code](./index.js)


