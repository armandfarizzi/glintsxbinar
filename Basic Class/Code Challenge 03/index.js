const products = require('./products.json');
const users = require('./users.json');
const fs = require('fs');
/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */

const http = require('http');
/* Code Here */
http
  .createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    let notFound = () => {
      response.write('{"message" : "404"}');
      response.end();
    };
    let writeResponse = (ctx) => {
      response.write(JSON.stringify(ctx));
      response.end();
    };
    switch (request.url.toLowerCase()) {
      case '/':
        writeResponse({ Hello: 'World' });
        break;
      /*
       *   Endpoints : /products/available
       *   Response  : Filtered products which stock not equal 0
       * */
      case '/products/available/':
      case '/products/available':
        writeResponse(products.filter((p) => p.stock !== 0));
        break;
      /*
       *   Endpoints : /products
       *   Response  : give all listed products
       *
       * */
      case '/products/':
      case '/products':
        writeResponse(products);
        break;
      /*
       *   Endpoints : /products/available
       *   Response  : Filtered products which stock not equal 0
       * */
      case '/users/':
      case '/users':
        let send = users;
        send['encrypted_password'] = 'encrypted-from-' + send['password'];
        delete send['password'];
        writeResponse(send);
        break;
      default:
        notFound();
    }
  })
  .listen(8080, function () {
    console.log('Listening to port 8080');
  });
