# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 4 & 5
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)
#### Javascript Data Type & Variable
- Primitives Data Type in Javascript : [(source)](https://developer.mozilla.org/en-US/docs/Glossary/Primitive)
    - string
    - number
    - bigint
    - boolean
    - undefined
    - symbol
- ```Let``` vs ```Var```[(source)](https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var)
In Javascript, for general convention ```let``` is more preferred than ```var```. ```var``` will allow the program to re-declare a variable. In contrast, ```let``` will throw an error when a program execute a re-declaration.  Avoiding re-declaration will benefit the programmers to reduce bug due to ambiguities.
#### Function
- Function declaration :
    - regular :
    ```
    function nameFunction(){
        // Do something...
    }
    ```
        
    - es6 style :
    ```
    const nameFunction = () => {
        // Do something...
    }
    ```
    
#### If Statements
- Purposes
- if-else VS swtich statement