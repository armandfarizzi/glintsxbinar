//  #  Variable Declaration
//  #  "name" declared
let name = 'Armand';  // "name" assigned to value 'Armand'

// #  Convention
// #  Mutable, camelCase
// #  Immutable variable of an alias, UPPERCASE_CONSTANT

let isPasswordValid = true;  // variable isPasswordValid coming with 3 phrases
const RED = '#FF0';   // constant RED is declared in uppercase
let isPasswordValid = "false!!!!";
// #  Variable cannot started with number
// #  e.g. 
// var 1Color = "this is invalid";

var color1 = "this is valid";
console.log(color1);

// #  "$", "_" also a valid name to declare variable
const $ = "myname armand";
const _ = "hello from '_' variables";
console.log($, _);


