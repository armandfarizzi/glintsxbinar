//  #  Function

//  #  Three way to create/define a function
//  #   1.
function functionName(){
	// Do Something
};

//  #   2.
const arrowFunction = () => {
    //  Do Something
}

//  #   3.
const anotherFunction = function(){
    //  Do Something
}

//  #   Procedure is a function that not return a value on called
//  #   In contrast, Real function give a value on return

//  #   arrow function vs other
//  #   arrow function return empty object from this
//  #   normal function return global object from this

//  #   Example
const fc = () => console.log("print arrow function this : ",this);  //  {}
function normalFc(){
    console.log("print normal function this : ", this);             //  { Global Object } 
} 
//  #   call predefined function
fc();
normalFc();



