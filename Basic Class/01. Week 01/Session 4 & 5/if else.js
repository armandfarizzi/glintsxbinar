const doesItRain = 1;
/*
 *	doesItRain is declared and given the value of 1 (an integer)
 **/
if (doesItRain) {	// doesItRain is true(?)
  console.log("use umbrella!");
}



function myFunction(a){
	return a && true
}

console.log(myFunction("GG"));

function example(x){
	if(x >= 10){
		return x / 2;
	} else {
		throw new Error("Input should greater than 10");
	}
j}


/*
 *	try uncomment //example(3) function below
 *	the program will return an error, and eventually stoping the program
 * 	
 * 	Try {} catch() {} block will allow the program to execute a handled error
 * 	without terminating the program from running.
 * 	In the end, "my name is armand" will be printed in terminal
 *  Try it! 
 *  go to terminal and type
 *  
 * 		> node if\ else.js
 **/

// let dontExecuteMe = example(3);

try{
  let executeMeSafely = example(2);
  console.log(executeMeSafely);
}

catch(err){
  console.log(err.message);
}

console.log("my name is armand")