![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
# Glints Academy - Batch 6 - Backend Class
## Summary from session 2 & 3
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)
#### All about git
- Introduction to git as source code management
- Initialize a git local repository
- Track a file change to git
- Allow git to exclude a certain file to track
- Golden rule in git-flow
    - NEVER PUSH TO BRANCH MASTER!! Request a merge to master instead.
    - Keep updated from remote repository by ```git pull origin master``` in working branch
- Exercise : 
    - push local repository to remote repository
    - collaborate with other to build and maintain a remote repository
