# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

## Summary from session 19

Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Model association with sequelize

As we create a User and Post model, we want associate them as one to many relationship respectively. In sequelize, association can be achieved by a few steps :

- Add a column in a Post table that contain a user_id that refer to "foreign_key"
- Modify User.associate and Post.associate object properties to having a sub property that defined which model is `belongsTo()` and which is `hasMany()`
- Sequelize can populate the user_id properties at Post column into the complete user data that match with user_id by calling include in querying options with sequelize

## Task

Check [apps-session19](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session19) branch
