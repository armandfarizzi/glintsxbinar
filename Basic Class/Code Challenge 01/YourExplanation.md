# Code Challenge Week 01

`greet()` function is a javascript function that allow the terminal to return the user a basic greeting that contain basic information such as name, address, and age. 

The `greet()` function take 3 arguments to be executed. Which is `name`, `address`, and `birthday`. 
> `name` and `address` is directly concatinated with the greeting messages.

>  to find the user `age`. Function will compare the current year, and substract it with the user `birthday` input. Then, `age` is concatinated with the previous greeting messages.

> to print the greeting messages. The function call `console.log()` with the greeting messages as the arguments. 