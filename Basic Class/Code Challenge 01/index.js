const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("How's your day?", answer => {
    console.log(answer);

    rl.close();
})

rl.on("close", ()=>{
    console.log("Bye");
    process.exit;
})
