# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 12
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Object Oriented Programming (OOP):
- Inheritance : 
  Every class can extends their feature to the parent classes, hence they will have same base behaviour and feature to the extending classes  
- Abstraction :
  Abstraction in OOP is a blue print for the relating classes, they (abstraction) can't be instantiated, they need to extended to children class, hence it concept is associated with inheritance in OOP  

## Task 

### In classes task

Given a general classes that has named ```Human```. Extend ```Human``` to ```Chef``` classes that allow to return a modified method from parent classes and give an extra property that could explain ```Chef``` attributes.

Completed in [index.js](index.js)

### Take home task

Given an *Abstraction* classes that has named [```Record```](task/Record.js). Extend ```Record``` to ```Book``` and ```Product``` classes so that may have ability to save a record which inherited from the *Abstraction* ```Record```.

Completed in [Book.js](task/Book.js) & [Product.js](task/Product.js)