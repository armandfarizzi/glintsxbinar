Object.defineProperty(Array.prototype, 'sample', {
  get: function () {
    return this[Math.floor(Math.random() * this.length)];
  },
});

class Human {
  static properties = ['name'];
  constructor(props) {
    this._validate(props);
    this.name = props.name;
  }
  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error('Constructor needs object to work with');

    this.constructor.properties.forEach((i) => {
      if (!Object.keys(props).includes(i))
        throw new Error(`missing properties`);
    });
  }
  introduce() {
    console.log(`Hello world, my name is ${this.name}`);
  }
  cook() {
    console.log(`${this.name} will cook you something..`);
  }
}

class Chef extends Human {
  static properties = [...super.properties, 'cuisines', 'type'];
  constructor(props) {
    super(props);
    this.cuisines = props.cuisines;
    this.type = props.type;
  }

  _validate(props) {
    super._validate(props);
    if (!Array.isArray(props.cuisines))
      throw new Error('cuisines must be an Array');
  }

  promote() {
    console.log(`Hi ${this.name} here, do you want ${this.cuisines.sample}?`);
  }

  introduce() {
    super.introduce();
    console.log(`And i am a ${this.type} chef`);
  }
  cook() {
    super.cook();
    console.log(`${this.cuisines.sample} ready! And it's very delicious!`);
  }
}

const Armand = new Chef({
  name: 'Armand',
  cuisines: ['Martabak', 'Gado gado', 'Steak'],
  type: 'Italian',
});

Armand.promote();
