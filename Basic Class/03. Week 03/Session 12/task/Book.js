const Record = require('./Record');
/*
  Book properties :
  - title
  - author
  - price
  - publisher
*/

class Book extends Record {
  static properties = ['title', 'author', 'price', 'publisher'];

  constructor(props) {
    super(props);
  }
}

const myBook = new Book({
  title: 'Steve Jobs',
  author: 'Walter Isaacson',
  price: 188000,
  publisher: 'Simon & Schuster (U.S.)',
});

const myAnotherBook = new Book({
  title: 'Naruto the Hokage',
  author: 'Sasuke the Betrayer',
  price: 144,
  publisher: 'Sakura the Healer',
});

console.log(
  myBook.update(1, {
    title: 'Mario Bros',
    author: 'Yoshi',
    price: 21124,
    publisher: 'Luigi',
  })
);
// console.log(myBook.all[myBook.all.length - 1].id + 1);

module.exports = Book;
