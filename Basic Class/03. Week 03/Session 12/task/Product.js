const Record = require('./Record');

/*
  Product properties :
    - name,
    - price,
    - stock
*/

class Product extends Record {
  static properties = ['name', 'price', 'stock'];
}

const myProduct = new Product({
  name: 'Sepatu Vans Old Skool',
  price: 1200000,
  stock: 3,
});

// myProduct.save();

module.exports = Product;
