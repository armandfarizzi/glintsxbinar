const fs = require('fs');

class Record {
  constructor(props) {
    if (this.constructor == Record)
      throw new Error("Can't instantiate from Record");

    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error('Props must be an object');

    this.constructor.properties.forEach((i) => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`);
    });
  }

  _set(props) {
    this.constructor.properties.forEach((i) => {
      this[i] = props[i];
    });
  }

  get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`).toString()
      );
    } catch {
      return [];
    }
  }

  find(id) {
    if (typeof id !== 'number') throw new Error('id is invalid');
    try {
      let data = JSON.parse(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`).toString()
      );
      return data.filter((c) => c.id == id)[0];
    } catch (err) {}
  }
  update(id, props) {
    if (typeof id !== 'number') throw new Error('id is invalid');
    this._validate(props);
    let data = JSON.parse(
      fs.readFileSync(`${__dirname}/${this.constructor.name}.json`).toString()
    );
    data.forEach((d) => {
      if (d.id == id) {
        for (const key in props) {
          d[key] = props[key];
        }
      }
    });
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify(data, null, 2)
    );
    return this.find(id);
  }

  delete(id) {
    let data = JSON.parse(
      fs.readFileSync(`${__dirname}/${this.constructor.name}.json`).toString()
    );
    data = data.filter((d) => d.id != id);
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify(data, null, 2)
    );
    return this.all;
  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify(
        [...this.all, { id: this.all[this.all.length - 1].id + 1, ...this }],
        null,
        2
      )
    );
  }
}

class User extends Record {
  static properties = ['email', 'password'];
}

let Fikri = new User({
  email: 'test01@mail.com',
  password: '123456',
});

let Fikri2 = new User({
  email: 'test02@mail.com',
  password: '123456',
});

// Fikri.save();
// Fikri2.save();

/*
  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher
*/

/*
  Product,
    name,
    price,
    stock
*/

module.exports = Record;
