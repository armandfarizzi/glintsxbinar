const { EventEmitter } = require('events')
const just = new EventEmitter();


//  Challenge
let users = {
    'test01@mail.com': 1,
    'armandfarizzi@gmail.com' : 5
};

just.on("Access", (data) =>{
    if(!users[data.email]){
        users[data.email] = 1
        just.emit("Send");
    }
    else if(users[data.email] >= 5){
        just.emit("Unauthorized", data.email);}
    else{
        users[data.email]++;
        just.emit("Send");
    }
    
});

just.on("Send", ()=>{
    console.log(users);
});

just.on("Unauthorized", (email)=>{
    console.log(`Unauthorized access, email has been sent to ${email}!`)
})

just.emit("Access", {email: 'armand@gmail.com'})
just.emit("Access", {email: 'armandfarizzi@gmail.com'})