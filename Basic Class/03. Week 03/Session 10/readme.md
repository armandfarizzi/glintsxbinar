# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 10
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Event
- Event is a set information that occur in program
- Event need to be registered first and then emitted to do the instruction
```
// Register
EventEmitter.on("EventName", ()=>{ // Register an Event named "EventName"
    // Do When "EventName" Occur
});
```
```
// Emitted
EventEmitter.Emit("EventName"); // Execute "EventName" listener above
```
- Emitter can be inside another listener
```
EventEmitter.on("NestedEmitter", (){
    // Do something..
    EventEmitter.Emit("EventName"); // "EventName" will occur afterwards
})
```
