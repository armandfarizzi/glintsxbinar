const Record = require('./Record');

class Book extends Record {
  static properties = {
    title: {
      type: 'string',
      required: true,
    },
    author: {
      type: 'string',
      required: true,
    },
    publisher: {
      type: 'string',
      required: true,
    },
  };
}

const myBook = new Book({
  title: 'Mybook4',
  author: 'My Author4',
  publisher: 'My Publisher4',
});

// myBook.save();
// console.log(myBook.delete(4));
// console.log(Book.find(5));
// console.log(myBook.update())
Book.update(1, {
  title: 'Mybook1..',
  author: 'My Author1..',
  publisher: 'My Publisher1..',
});
