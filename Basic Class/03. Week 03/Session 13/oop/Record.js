const fs = require('fs');

module.exports = class Record {
  constructor(props) {
    if (this.constructor == Record)
      throw new Error("Can't instantiate from Record");

    this._set(props);
  }

  _set(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error('Props must be an object');

    const properties = this.constructor.properties;

    for (let i in properties) {
      // Assign the properties as instance props in that class
      this[i] = props[i];

      if (!this[i] && properties[i].required)
        throw new Error(`${i} is required`);

      if (typeof this[i] !== properties[i].type)
        throw new Error(`${i} should be ${properties[i].type}`);
    }
  }

  static save(ctx) {
    fs.writeFileSync(`${__dirname}/db/${this.name}.json`, ctx);
  }

  static get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/db/${this.name}.json`).toString()
      );
    } catch {
      return [];
    }
  }

  static find(id) {
    const inst = new this(this.all.filter((i) => i.id == id)[0]);
    return inst;
  }

  static update(id, props) {
    if (typeof id !== 'number') throw new Error('id is invalid');
    let data = JSON.parse(
      fs.readFileSync(`${__dirname}/db/${this.name}.json`).toString()
    );
    data.forEach((d) => {
      if (d.id == id) {
        for (const key in props) {
          d[key] = props[key];
        }
      }
    });

    this.save(JSON.stringify(data, null, 2));
    return this.find(id);
  }

  delete(id) {
    this.constructor.save(
      JSON.stringify(
        [...this.constructor.all.filter((i) => i.id !== id)],
        null,
        2
      )
    );
  }

  save() {
    this.constructor.save(
      JSON.stringify(
        [
          ...this.constructor.all,
          { id: this.constructor.all.length + 1, ...this },
        ],
        null,
        2
      )
    );
  }
};
