const http = require('http');
const PORT = 3000;
const app = http.createServer((req, res) => {
  switch (req.url) {
    case '/':
      res.write('Hello World\n');
      break;

    case '/products':
      res.write('This is a product\n');
      break;

    case '/error':
      try {
        throw new Error('This is error!');
      } catch (err) {
        res.writeHead(500);
        res.write(err.message);
      }
      break;

    default:
      res.writeHead(404);
      res.write('404 Not Found!\n');
  }

  res.end();
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
