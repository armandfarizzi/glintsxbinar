# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 13
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### OOP (Polymorphism & Encapsulation):
- Polymorphism : 
  Polymorphism has a strong relation with abstraction. Polymorphism means a class can be derived into multiple form and be used accordingly. Example: ```class Character``` is being used. ```Character``` could extended into ```Npc```, ```Player``` or ```Enemy```. Hence, ```Character``` has Polymorphism which it could be formed to ```Npc``` or etc. And the ```Character``` itself is a **Abstraction** for the other class.  
- Encapsulation :
  Encapsulation means a class is have the ability to hide a certain information so it can be stored securely. 

### Promises:
- Promise is introduced as asyncronous way in term of program flow.
- Promise will let the succeeding code to run first while the priority inside of it will be reduced.
- The promise will return a **resolved** or **rejected** value based on condition given.

### Http module (node.js)
- HTTP Module is a built-in node.js package that allowing us to handle every http action such as requesting to a url, creating a server and many more.


## Task 

Completed ```update()``` method from ```Record``` class.

Code in [Record.js](oop/Record.js)