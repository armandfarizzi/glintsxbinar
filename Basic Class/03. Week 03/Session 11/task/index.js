class Human {

    static isLiving = true;
    /* 
    Requirements :
    - Greet another Human
    - Marry another Human
    - Introduce themself
    - Learn new language
    - If person a tries to marry person b, make sure they've both understand the same language, 
      otherwise, one of them will learn a new languange that both will understand.
    */
    constructor(name, language) {
        this.name = name; // 
        this.language = language; //
    }
    isMarried = false; 
    /*
        - Greet another Human
    */
    greet(target) {
        console.log(`Hi ${target.name}! my name is ${this.name}!`);
    }

    /*
        - Marry another Human
    */
    #doMarry = function(target){
        this.isMarried = true;
        target.isMarried = true;
        this.partner = target.name;
        target.partner = this.name;
        console.log(`\n${this.name} married with ${target.name}`)
    }
    /*
        - Introduce themself
    */
    introduce(){ 
        console.log(`Hello! my name is ${this.name}!`)
    }    
  
    /*
        - Learn new language
    */
    learnLang(i){
        console.log(`${this.name} before Learn : `, this.language);
        this.language.push(i);
        console.log(`${this.name} learn :`, i);
    }
    /*
        - Marriage must understand at least one language from each other
    */
    checkLang(target){
        let value = false
        this.language.forEach((i)=>{
            if(target.language.indexOf(i) != -1){
                value = true
            }
        })
        return value
    }
    learnSomeoneLang(target){
        this.learnLang(target.language[Math.floor(Math.random()*(target.language.length))]);
        console.log(`${this.name} Language: `,this.language, "\nPartner Language :", target.language);
    }

    marry(target){
        console.log(`${this.name} will marry ${target.name}`)
        if(!target.isMarried && !this.isMarried){
            if(!this.checkLang(target)){
                console.log("Learn first!");
                this.learnSomeoneLang(target);
                this.isMarried = true;
            }
            console.log("Let's Marry now");
            this.#doMarry(target);            
        } else {
            console.log("Can't marry yet!")
        }
    }
    
}

let Armand = new Human("Armand", ["JavaScript"]);
let Amanda = new Human("Amanda", ["Ruby", "Scala", "Golang"]);
let AnotherAmanda = new Human("AmandaAmanda", ["JavaScript", "Python"]);



module.exports = {Armand, Amanda, AnotherAmanda};