# Glints Academy - Batch 6 - Backend Class
![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")
## Summary from session 11
Class by : [@fnurhidayat](https://gitlab.com/fnurhidayat)

### Object Oriented Programming (OOP):
- OOP is a computer programming paradigm that based on the concept of ```Object```.
- ```class``` is an object blue print. ```class``` help us to shorten the process of declaring multiple same object.
```
class Person {
    // Person class created
}
```
- ```class``` have property & method. property is basically a variable that class own, and method is a function inside a class
- **Static property** is a constant that every class defined own. **Instance property** is defined when a new instance of the class is created.
```
class Person {
    static isDead = false;  // every person has isDead Value
    constructor(name, address){ // constructor is a function that occur every class is created.
        this.name = name;
        this.address = address
    } // every person have different name & address
}

// create a new person class
const Armand = new Person("Armand", "Bandung")
```

## Task
Created a ```Human```  that could :
- Greet another Human

![clean data](../../.assets/task-6-greet.png)

- Marry another Human

![clean data](../../.assets/task-6-marry-1.png)

- Introduce themself

![clean data](../../.assets/task-6-introduce.png)

- Learn new language

![clean data](../../.assets/task-6-learn.png)

- If person a tries to marry person b, make sure they've both understand the same language, otherwise, one of them will learn a new languange that both will understand.
  
![clean data](../../.assets/task-6-marry-2.png)

[jump to Human class](./task/index.js) 
