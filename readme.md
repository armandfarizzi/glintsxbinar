# Glints Academy - Batch 6 - Backend Class

![alt text](https://gitlab.com/binar-x-glints-academy/batch-6/backend/materials/-/raw/material/.assets/BinarXGlints.png "Glints Academy")

### :new: Session 19 Task : [Jump to directory](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session19) :new:

### :boom: Code Challange 04 @ branch [apps-cc4](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-cc4) :boom:

# Summary

## Week 01

- [Session 1](Basic%20Class/01.%20Week%2001/Session%201/summary.md)
- [Session 2 & 3](Basic%20Class/01.%20Week%2001/Session%202%20&%203/summary.md)
- [Session 4 & 5](Basic%20Class/01.%20Week%2001/Session%204%20&%205/summary.md)

## week 02

- [Session 6 & 7](Basic%20Class/02.%20Week%2002/Session%206%20&%207/summary.md)
- [Session 8 & 9](Basic%20Class/02.%20Week%2002/Session%208%20&%209/summary.md)

## Week 03

- [Session 10](Basic%20Class/03.%20Week%2003/Session%2010/readme.md)
- [Session 11](Basic%20Class/03.%20Week%2003/Session%2011/readme.md)
- [Session 12](Basic%20Class/03.%20Week%2003/Session%2012/summary.md)
- [Session 13](Basic%20Class/03.%20Week%2003/Session%2013/summary.md)

## Week 04

- [Session 14](Basic%20Class/04.%20Week%2004/Session%2014/summary.md)
- [Session 15](Basic%20Class/04.%20Week%2004/Session%2015/summary.md)
- [Session 16](Basic%20Class/04.%20Week%2004/Session%2016/summary.md)
- [Session 17](Basic%20Class/04.%20Week%2004/Session%2017/summary.md) & [Task](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session17)
- [Session 18](Basic%20Class/04.%20Week%2004/Session%2018/summary.md) & [Task](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session18)

## Week 05

- [Session 19](Basic%20Class/05.%20Week%2005/Session%2019/summary.md) & [Task](https://gitlab.com/armandfarizzi/glintsxbinar/-/tree/apps-session19)
